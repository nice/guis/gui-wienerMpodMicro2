<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>HV channel supervision status</name>
  <x>700</x>
  <y>200</y>
  <width>560</width>
  <height>215</height>
  <widget type="group" version="2.0.0">
    <name>Supervision limits for HV channel $(CHAN) at slot </name>
    <x>10</x>
    <y>10</y>
    <width>540</width>
    <height>195</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <widget type="label" version="2.0.0">
      <name>lblMinSenseVoltageFailR</name>
      <text>Minimum sense (V):</text>
      <x>18</x>
      <y>35</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblMaxSenseVoltageFailR</name>
      <text>Maximum sense (V):</text>
      <x>18</x>
      <y>55</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblMaxTerminalVoltageFailR</name>
      <text>Maximum terminal (V):</text>
      <x>18</x>
      <y>75</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblMaxCurrentFailR</name>
      <text>Maximum sense (C):</text>
      <x>18</x>
      <y>95</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblMaxPowerFailR</name>
      <text>Maximum power:</text>
      <x>18</x>
      <y>115</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblMaxTemperatureFailR</name>
      <text>Temperature:</text>
      <x>18</x>
      <y>135</y>
      <width>130</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMinSenseVoltageFailR</name>
      <pv_name>$(DEV)::$(CHAN):MinSenseVoltageFailR</pv_name>
      <x>160</x>
      <y>35</y>
      <width>320</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Sense voltage too low</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMaxSenseVoltageFailR</name>
      <pv_name>$(DEV)::$(CHAN):MaxSenseVoltageFailR</pv_name>
      <x>160</x>
      <y>55</y>
      <width>320</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Sense voltage too high</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMaxPowerFailR</name>
      <pv_name>$(DEV)::$(CHAN):MaxPowerFailR</pv_name>
      <x>160</x>
      <y>115</y>
      <width>320</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Output power too high</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMaxCurrentFailR</name>
      <pv_name>$(DEV)::$(CHAN):MaxCurrentFailR</pv_name>
      <x>160</x>
      <y>95</y>
      <width>320</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Current too high</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMaxTemperatureFailR</name>
      <pv_name>$(DEV)::$(CHAN):MaxTemperatureFailR</pv_name>
      <x>160</x>
      <y>135</y>
      <width>220</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Heat sink temperature too high</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledMaxTerminalVoltageFailR</name>
      <pv_name>$(DEV)::$(CHAN):MaxTerminalVoltageFailR</pv_name>
      <x>160</x>
      <y>75</y>
      <width>320</width>
      <height>18</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_label>Terminal voltage too high</on_label>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtTempR</name>
      <pv_name>$(DEV)::$(CHAN):TempR</pv_name>
      <x>389</x>
      <y>135</y>
      <width>82</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <transparent>true</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
</display>
