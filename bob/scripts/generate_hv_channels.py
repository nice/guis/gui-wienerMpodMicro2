# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to parse description obtained by SNMP
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Standard libraries
# -----------------------------------------------------------------------------
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Specific libraries on CS-Studio
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.runtime.pv import PVFactory

# -----------------------------------------------------------------------------
# Wiener MPOD has 10 slots for HV/LV boards
# -----------------------------------------------------------------------------
MAX_SLOTS = 10

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    # -----------------------------------------------------------------------------
    # This Python script is attached to a display
    # and triggered by loc://initial_trigger$(DID)(1)
    # to execute once when the display is loaded.
    # -----------------------------------------------------------------------------
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        moduleDesc = []
        moduleDesc0             = PVUtil.getString(pvs[1])
        moduleDesc.append(moduleDesc0)
        moduleDesc1             = PVUtil.getString(pvs[2])
        moduleDesc.append(moduleDesc1)
        moduleDesc2             = PVUtil.getString(pvs[3])
        moduleDesc.append(moduleDesc2)
        moduleDesc3             = PVUtil.getString(pvs[4])
        moduleDesc.append(moduleDesc3)
        moduleDesc4             = PVUtil.getString(pvs[5])
        moduleDesc.append(moduleDesc4)
        moduleDesc5             = PVUtil.getString(pvs[6])
        moduleDesc.append(moduleDesc5)
        moduleDesc6             = PVUtil.getString(pvs[7])
        moduleDesc.append(moduleDesc6)
        moduleDesc7             = PVUtil.getString(pvs[8])
        moduleDesc.append(moduleDesc7)
        moduleDesc8             = PVUtil.getString(pvs[9])
        moduleDesc.append(moduleDesc8)
        moduleDesc9             = PVUtil.getString(pvs[10])
        moduleDesc.append(moduleDesc9)

        # -----------------------------------------------------------------------------
        # Loading number of channels for installed modules from PVs
        # -----------------------------------------------------------------------------
        display         = widget.getDisplayModel()
        old_macros      = display.getEffectiveMacros()
        device          = old_macros.getValue('DEV')
        card_nr         = old_macros.getValue('SLOT')
        nbrOfchannels   = int(moduleDesc[int(card_nr)].split(',')[2])
        #logger.info("moduleChan: %d" % int(moduleDesc[int(card_nr)].split(',')[2]))
        #logger.info("card_nr: %s" % str(card_nr))
        #logger.info("nbrOfchannels: %s" % str(nbrOfchannels))

        channels = []
        for i in range(nbrOfchannels):
            formattedChannel = str("%d" % (i))
            # obtain the read value fo group for this channel...
            #     > the PV is something like: Utg-VIP:Det-PSU-04:5:00:GroupR
            #     > generated from macros: $(DEV):$(SLOT):$(CHAN):GroupR
            #groupReadPvName = "%s:%s:%s:GroupR" % (device, card_nr, formattedChannel)
            # -----------------------------------------------------------------
            # WienerMPOD Micro-2 crate is different
            # -----------------------------------------------------------------
            groupReadPvName = "%s::%s:GroupR" % (device, formattedChannel)
            pvEpicsGroupR = 0
            try:
                groupReadPv = PVFactory.getPV(groupReadPvName)
                pvEpicsGroupR = PVUtil.getInt(groupReadPv)
                # to avoid memory leak...
                PVFactory.releasePV(groupReadPv)
                #logger.info("pvEpicsGroupR: %d" % pvEpicsGroupR)
            except:
                logger.warning("Error trying to read %s" % groupReadPvName)
            channels.append({
                            'NAME' : "Channel %d" % (i),
                            'CHAN' : formattedChannel,
                            'GROUP': str(max(0, pvEpicsGroupR -1))      # should be at least 'zero'
                            })

        # -----------------------------------------------------------------------------
        # Create display:
        # -----------------------------------------------------------------------------
        # For each 'channel', add one embedded display
        # which then links to the single_channel.bob
        # with the macros of the device.
        embedded_width  = 760
        embedded_height = 20

        def createInstance(x, y, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            embedded.setPropertyValue("file", "single_channel.bob")
            return embedded

        display = widget.getDisplayModel()
        # coordinates of where the headers stop
        startX = 5
        startY = 110
        # resolution of display
        resX = 760
        resY = 600
        for i in range(len(channels)):
            x = startX
            y = startY + (embedded_height * (i))
            instance = createInstance(x, y, channels[i])
            display.runtimeChildren().addChild(instance)
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()
