# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to parse description obtained by SNMP
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Wiener MPOD has 10 slots for HV/LV boards
# -----------------------------------------------------------------------------
MAX_SLOTS = 10

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        moduleIndex = []
        moduleIndex0             = PVUtil.getString(pvs[1])     # $(DEV):0:iModuleIndexR
        moduleIndex.append(moduleIndex0)
        moduleIndex1             = PVUtil.getString(pvs[2])     # $(DEV):1:iModuleIndexR
        moduleIndex.append(moduleIndex1)
        moduleIndex2             = PVUtil.getString(pvs[3])     # $(DEV):2:iModuleIndexR
        moduleIndex.append(moduleIndex2)
        moduleIndex3             = PVUtil.getString(pvs[4])     # $(DEV):3:iModuleIndexR
        moduleIndex.append(moduleIndex3)
        moduleIndex4             = PVUtil.getString(pvs[5])     # $(DEV):4:iModuleIndexR
        moduleIndex.append(moduleIndex4)
        moduleIndex5             = PVUtil.getString(pvs[6])     # $(DEV):5:iModuleIndexR
        moduleIndex.append(moduleIndex5)
        moduleIndex6             = PVUtil.getString(pvs[7])     # $(DEV):6:iModuleIndexR
        moduleIndex.append(moduleIndex6)
        moduleIndex7             = PVUtil.getString(pvs[8])     # $(DEV):7:iModuleIndexR
        moduleIndex.append(moduleIndex7)
        moduleIndex8             = PVUtil.getString(pvs[9])     # $(DEV):8:iModuleIndexR
        moduleIndex.append(moduleIndex8)
        moduleIndex9             = PVUtil.getString(pvs[10])    # $(DEV):9:iModuleIndexR
        moduleIndex.append(moduleIndex9)
        moduleName = []
        moduleName0             = PVUtil.getString(pvs[11])     # $(DEV):0:ModuleNameR
        moduleName.append(moduleName0)
        moduleName1             = PVUtil.getString(pvs[12])     # $(DEV):1:ModuleNameR
        moduleName.append(moduleName1)
        moduleName2             = PVUtil.getString(pvs[13])     # $(DEV):2:ModuleNameR
        moduleName.append(moduleName2)
        moduleName3             = PVUtil.getString(pvs[14])     # $(DEV):3:ModuleNameR
        moduleName.append(moduleName3)
        moduleName4             = PVUtil.getString(pvs[15])     # $(DEV):4:ModuleNameR
        moduleName.append(moduleName4)
        moduleName5             = PVUtil.getString(pvs[16])     # $(DEV):5:ModuleNameR
        moduleName.append(moduleName5)
        moduleName6             = PVUtil.getString(pvs[17])     # $(DEV):6:ModuleNameR
        moduleName.append(moduleName6)
        moduleName7             = PVUtil.getString(pvs[18])     # $(DEV):7:ModuleNameR
        moduleName.append(moduleName7)
        moduleName8             = PVUtil.getString(pvs[19])     # $(DEV):8:ModuleNameR
        moduleName.append(moduleName8)
        moduleName9             = PVUtil.getString(pvs[20])     # $(DEV):9:ModuleNameR
        moduleName.append(moduleName9)

        # -----------------------------------------------------------------------------
        # Loading installed modules from PVs
        # -----------------------------------------------------------------------------
        # Wiener MPOD has 10 slots for HV/LV boards
        # -----------------------------------------------------------------------------
        # Determine card type
        hvCards = []
        lvCards = []

        for idx, val in enumerate(moduleName):
            if "iseg" in val:
                # High voltage
                hvCards.append(idx)
            elif "WIENER" in val:
                # Low voltage
                lvCards.append(idx)
            #logger.info("moduleName: %s, moduleIndex: %s" % (val, moduleIndex[idx]))

        # Filling in all boards
        cards = []

        for i in range(MAX_SLOTS):
            cards.append({
                         'NAME' : "BOARD %d" % (i),
                         'SLOT' : "%d" % i
                         })

        # Define widget properties
        def createInstance(x, y, id, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            if id in hvCards:
                embedded.setPropertyValue("file", "card_hv.bob")
            elif id in lvCards:
                embedded.setPropertyValue("file", "card_lv.bob")
            else:
                embedded.setPropertyValue("file", "card_none.bob")
            return embedded

        # Create widgets
        embedded_width = 70
        embedded_height = 400
        gap = 1
        startX = 30
        startY = 260
        display = widget.getDisplayModel()
        for i in range(len(cards)):
            x = startX + ( (i) * embedded_width + (i) * gap )
            y = startY
            instance = createInstance(x, y, (i), cards[i])
            display.runtimeChildren().addChild(instance)
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()