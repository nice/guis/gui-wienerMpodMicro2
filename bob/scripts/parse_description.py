# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to parse description obtained by SNMP
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
#from pvaccess import BOOLEAN, BYTE, UBYTE, SHORT, USHORT, INT, UINT, LONG, ULONG, FLOAT, DOUBLE, STRING, PvObject, PvaServer

import sys, time

from time import sleep
from array import array
from jarray import zeros


# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    try:
        # -------------------------------------------------------------------------
        # logical representation of PVs
        # -------------------------------------------------------------------------
        description = PVUtil.getString(pvs[0])
        description = description.replace('\"','')
        description = description.replace('[','')
        description = description.replace(']','')
        description = description.replace('(','')
        description = description.replace(')','')
        #logger.info("description: %s" % description)
        # -------------------------------------------------------------------------
        # other input PVs, not triggers
        # pvs[1]: description
        # pvs[2]: descMPOD
        # pvs[3]: descMPODslave
        # pvs[4]: descMPOD-BL
        # -------------------------------------------------------------------------
        # splitting the string
        # > it is expected something like this (an array):
        # > [" WIENER MPOD (2785032, MPOD 2.1.3496.0,", "MPODslave 1.09, MPOD-BL 2.243)", "", "", "", "", "", "", "", "",\
        #    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",\
        #    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",\
        #    "", "", "", ""]
        # -------------------------------------------------------------------------
        # first element: " WIENER MPOD (2785032, MPOD 2.1.3496.0,"
        tempWIENER = description.split(',')[0].split(' ')
        #logger.info(str(tempWIENER))
        if ('WIENER' in tempWIENER[1]):
            pvs[1].setValue('%s %s' % (tempWIENER[1], tempWIENER[2]))

        #pvs[1].setValue(description.split(',')[0].split('(')[0].strip())
        #logger.info(str(description.split(',')[0].split('(')))
        tempMPOD = description.split(',')[1].split(' ')
        #logger.info(str(tempMPOD))
        if ('MPOD' in tempMPOD[1]):
            pvs[2].setValue(tempMPOD[2])
        # second element: "MPODslave 1.09, MPOD-BL 2.243)"
        tempMPODslave = description.split(',')[3].split(' ')
        #logger.info(str(tempMPODslave))
        if ('MPODslave' in tempMPODslave[1]):
            pvs[3].setValue(tempMPODslave[2])
        tempMPOD_BL = description.split(',')[4].split(' ')
        #logger.info(str(tempMPOD_BL))
        if ('MPOD-BL' in tempMPOD_BL[1]):
            pvs[4].setValue(tempMPOD_BL[2])
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()