# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Wiener MPOD interface; procedure to update the channels groupping
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# DET  - anders.lindholsson@esss.se
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
#from pvaccess import BOOLEAN, BYTE, UBYTE, SHORT, USHORT, INT, UINT, LONG, ULONG, FLOAT, DOUBLE, STRING, PvObject, PvaServer

import sys, time, datetime

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def setGroupProcedure():
    try:
        # -------------------------------------------------------------------------
        # logical representation of PVs
        # -------------------------------------------------------------------------
        selectedGroup = PVUtil.getInt(pvs[0])        # loc://selectGroup$(CHAN)
        # logger.info("selectedGroup %d " % selectedGroup)
        groupSet    = PVUtil.getInt(pvs[1])          # $(DEV):$(SLOT):$(CHAN):GroupS
        # logger.info("PV %s " % str(pvs[1]))
        pvs[1].setValue(selectedGroup +1)

    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
setGroupProcedure()